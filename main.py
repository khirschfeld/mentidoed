import time

import requests
import threading


def main():
    """CONSTANTS"""
    text_options: list = ["wordcloud", "open"]
    botable_options: list = ["choices", "scales"] + text_options

    poll_pin = input("Poll Pin ~> ")
    poll_details = get_poll_details(poll_pin)

    # List all slides and let the user choose which to bot
    relevant_attributes = {}
    for question in poll_details.get("questions"):
        # Make sure to only display botable ones
        if question.get('type') in botable_options:
            print(f"{question.get('id')} ~ {question.get('slug')} ~ {question.get('type')}")
            # Save into custom list to make content more readable
            relevant_attributes.update(
                {question.get('id'): {"type": question.get('type'), "choices": question.get('choices')}})

    # Make sure the chosen ID exists
    chosen_id = ""
    while chosen_id not in relevant_attributes:
        chosen_id = input("Choose an id ~> ")

    body = get_body_from_user(relevant_attributes[chosen_id])

    # Python go "b"+"r"*100
    desired_thread_count = int(input("How many Threads do you want to run? ~> "))
    for i in range(0, desired_thread_count):
        threading.Thread(target=vote_sender, args=(chosen_id, body, i)).start()
    print("Up and running")


def get_body_from_user(poll):
    # Get and parse input for poll according to type of question
    if poll.get("type") == "choices":
        # Print all possible answers and their IDs
        for option in poll.get("choices"):
            print(f"{option.get('id')} ~ {option.get('label')}")
        vote_content = int(input("Choose an id ~> "))
    elif poll.get("type") == "scales":
        vote_content = {}
        # Get numeric value for every slider
        for option in poll.get("choices"):
            print(f"{option.get('id')} ~ {option.get('label')}")
            slider_value = int(input("Value for this Slider ~> "))
            vote_content.update({option.get('id'): [slider_value, 1]})
    else:
        # Else assumes prior use of all unique_options
        vote_content = input("Choose your text ~> ")
    return {"vote": vote_content, "type": poll.get('type')}


def vote_sender(poll_id: str, body: dict, thread_id: id):
    last = time.time()
    counter = 0
    while True:
        identifier = requests.post('https://www.menti.com/core/identifiers',
                                   headers={
                                       "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/117.0"}).json().get(
            "identifier")
        response = requests.post(f'https://www.menti.com/core/votes/{poll_id}',
                                 headers={
                                     "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/117.0",
                                     "X-Identifier": identifier},
                                 json=body)
        counter = counter + 1
        print(f"Thread: {thread_id} Request nr.: {counter} RPM: {int(60 / (time.time() - last))} Response: {response}")
        last = time.time()


def get_poll_details(poll_id: str):
    response = requests.get(f"https://www.menti.com/core/vote-ids/{poll_id}/series",
                            headers={
                                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/117.0"})
    return response.json()


if __name__ == "__main__":
    main()
